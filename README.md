## KakiExam（参天大树考试系统）3.0

#### 介绍
基于ssm垂直架构的考试系统，分为前台与后台，移动端(新增) 三大模块，前台具有，用户注册，登录，答题，计分功能，后台具有题目管理，卷纸录入功能，移动端基于H5开发，可移植至微信小程序，或封装为akp文件。

#### 软件架构
软件架构说明

后台采用：jdk1.8,spring,springMVC,mybatis,通用mapper ,lombook,shiro等

前端采用：vue,jQ,bootstrap，Echarts等


#### 安装教程

1. 开发工具：ieda

2. 数据库：mysql5.7及以上(注意：我的本地是mysql8，但代码中没有涉及到mysql8的语法。

        在sql导出的脚本中可能8与5.7有做不同，当出现导入失败时，可以使用一句一句复制进去)
    
        数据库配置在application中配置地址

3. 新增头像上传功能，可以使用nginx，或者tenginx作为图片服务器，配置访问路径和真是文件路径

4. 部署可在采用一台tomcat即可

#### 近期更新说明
1.更新web网页端的整体布局，可点击切换经典版回到原来的布局，并且在原布局的导航栏中加入“切换优化版”的布局模式
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200429133533200.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

#### 页面展示
**前台登录页面（面向考试者）**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201337288.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)
注册页面
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201845916.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020041220185422.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

考试页面
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020042913452371.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)


![在这里插入图片描述](https://img-blog.csdnimg.cn/20200429134609563.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

查看历史考试记录
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200429134704620.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

娱乐模块
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200429134757943.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)



**后台登录页面（面向管理员，老师等操作者）**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201559931.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)


后台主页面![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201609626.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

题目录入(新增或修改，删除题目)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201639524.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

添加卷纸，添加题目
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412202422158.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020041220170453.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

**移动端，微信端(新增)**
登录及首页
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201733739.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

考试模块
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201748627.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)


查看历史考试记录模块

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201756260.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)

学习资料模块（新增）
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200412201802359.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ0OTg4MDg4,size_16,color_FFFFFF,t_70)
#### 参与贡献

1.  Fork 本仓库
2.  感谢大家关注，点赞，Fork  ，持续更新中
3.  各位伙伴的star一下就是对我最大的鼓励


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


欢迎点赞关注收藏哦 ，码云搜索[KakiNakajima](https://search.gitee.com/?skin=rec&type=repository&q=KakiNakajima&repo=&reponame=)